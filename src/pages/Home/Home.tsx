import React, { useEffect } from 'react';
import Flicking from '@egjs/react-flicking';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';
import _ from 'lodash';

import { Col, Row, Tag } from 'antd';

import { BottomSheet } from '../../components/BottomSheet/BottomSheet';
import { CatchCard } from '../../components/CatchCard/CatchCard';
import { pokemonAvatar } from '../../utils/functions/pokemon-avatar';
import { Pokedex } from '../../containers/Pokedex/Pokedex';
import { Teams } from '../../containers/Teams/Teams';

import { ModalConstants } from '../../utils/contansts/modal-constant';
import { Dispatch, RootState } from '../../store/store';

import './Home.less';

import FavoriteWidget from '../../components/FavoriteWidget/FavoriteWidget';

const funcName = 'home';

export const Home = () => {
    const dispatch = useDispatch<Dispatch>();
    const { catch: catchWidget, favorite: favoriteWidget } = useSelector((state: RootState) => state.widget);
    const { pokemonList } = useSelector((state: RootState) => state.pokemon);
    const teams = useSelector((state: RootState) => state.teams);

    useEffect(() => {
        _.attempt(dispatch.widget.initiateCatchWidget);
        _.attempt(dispatch.widget.initiateFavoriteWidget);
        _.attempt(dispatch.app.pageStatus, 'loaded');
    }, [dispatch]);

    return (
        <div className={classNames(funcName)}>
            <div className={classNames(`${funcName}-title`)}>
                <span>know your</span>
                <div className={'skew-container'}>
                    <span>favorite</span>
                    <div className={'skew'}>pokemon</div>
                </div>
            </div>

            <FavoriteWidget
                title={favoriteWidget?.pokemon.name}
                subtitle={favoriteWidget?.pokemonSpecies.flavor_text_entries[9].flavor_text}
                avatar={pokemonAvatar(favoriteWidget?.pokemon.id.toString())}
            />

            <Row className={classNames(`${funcName}-number-total`)} justify={'center'} align={'middle'}>
                <Col span={12} className={classNames(`col-container`)}>
                    <Tag className={classNames(`tag-container`)} color={'rgb(43 189 255)'}>
                        <span className={classNames(`tag-content`)}>{Object.keys(pokemonList).length} of 649 pokedex</span>
                    </Tag>
                </Col>
                <Col span={12} className={classNames(`col-container`)}>
                    <Tag className={classNames(`tag-container`)} color={'rgb(40 247 22)'}>
                        <span className={classNames(`tag-content`)}>{Object.keys(teams).length} catched</span>
                    </Tag>
                </Col>
            </Row>

            <div className={classNames(`${funcName}-catch`)}>
                <span>Available To Catch : </span>
            </div>

            <Flicking className="flicking flicking0" gap={10} bound={true} zIndex={5}>
                {React.Children.toArray(
                    Object.values(catchWidget).map((pokemon) => (
                        <CatchCard
                            name={_.isNull(pokemon) ? '' : pokemon.name}
                            avatar={_.isNull(pokemon) ? '' : pokemonAvatar(pokemon.id.toString())}
                            loading={_.isNull(pokemon)}
                            onClick={() =>
                                dispatch.modal.openModal({ type: ModalConstants.CATCH, data: pokemon, closable: true })
                            }
                        />
                    ))
                )}
            </Flicking>

            <BottomSheet
                tabs={[
                    {
                        title: <span>POKEDEX</span>,
                        content: <Pokedex />,
                    },
                    {
                        title: <span>TEAMS</span>,
                        content: <Teams />,
                    },
                ]}
            />
        </div>
    );
};

export default Home;
