import React, { useEffect } from 'react';
import { useRequest } from '@umijs/hooks';
import { useLocation, useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';
import _ from 'lodash';

import { Button } from 'antd';
import { Grid } from 'antd-mobile';
import { pokemonAvatar } from '../../utils/functions/pokemon-avatar';
import { SkewFooter } from '../../components/SkewFooter/SkewFooter';
import { ReactComponent as Stats } from '../../assets/statistics.svg';

import { ModalConstants } from '../../utils/contansts/modal-constant';
import { Dispatch, RootState } from '../../store/store';

import './PokemonInformation.less';

const funcName = 'pokemon-information';

export const PokemonInformation = () => {
    const { state } = useLocation();
    const { pokemonName } = useParams();
    const navigate = useNavigate();
    const dispatch = useDispatch<Dispatch>();
    const pokemon = useSelector((state: RootState) => state.pokemonInformation);
    const teams = useSelector((state: RootState) => state.teams);
    const { run } = useRequest(dispatch.pokemonInformation.fetchPokemonInformation, { manual: true });

    useEffect(() => {
        if (!pokemon[pokemonName]) {
            _.attempt(run, pokemonName);
            _.attempt(dispatch.app.pageStatus, 'loaded');
        }
    }, []);

    useEffect(() => {
        if (pokemon[pokemonName]) {
            dispatch.app.pageStatus('loaded');
        }
    }, [pokemon]);

    useEffect(() => {
        if (state !== null && teams[state['member'].nickname] === undefined) {
            navigate('/');
        }
    }, [teams]);

    return (
        <>
            <div className={classNames(funcName)}>
                <div className={`${classNames(funcName)}-avatar-container`}>
                    {pokemon[pokemonName] && (
                        <img
                            alt={'pokemon-ability'}
                            height={250}
                            width={250}
                            src={pokemonAvatar(pokemon[pokemonName]?.pokemon.id.toString())}
                        />
                    )}
                </div>

                <div className={`${classNames(funcName)}-id-container`}>
                    <span> # {_.padStart(pokemon[pokemonName]?.pokemon.id.toString(), 3, '0')}</span>
                </div>

                <span className={`${classNames(funcName)}-name-container`}>{pokemon[pokemonName]?.pokemon.name}</span>

                <Grid
                    data={pokemon[pokemonName]?.pokemon.types.map(({ type }) => ({
                        icon: `${process.env.PUBLIC_URL}/assets/pokemon/types/${type.name}.png`,
                    }))}
                    renderItem={(item) => (
                        <div className={`${classNames(funcName)}-types-container`}>
                            <img alt={'type'} height={40} width={40} src={item?.icon} />
                        </div>
                    )}
                    itemStyle={{
                        height: 50,
                    }}
                    columnNum={6}
                    hasLine={false}
                />

                <span className={`${classNames(funcName)}-fact-container`}>FACT</span>
                <span className={`${classNames(funcName)}-description-container`}>
                    {pokemon[pokemonName]?.pokemonSpecies.flavor_text_entries[12].flavor_text}
                </span>
                {state !== null && (
                    <Button
                        className={`${classNames(funcName)}-release`}
                        type={'primary'}
                        onClick={() => {
                            dispatch.modal.openModal({
                                type: ModalConstants.RELEASE,
                                data: { nickname: state['member'].nickname, pokemon: pokemon[pokemonName].pokemon },
                                closable: true,
                            });
                        }}
                    >
                        Release
                    </Button>
                )}
            </div>

            <SkewFooter
                dataSource={[
                    { title: 'STATS', icon: <Stats className={classNames(`${funcName}-skew-footer-icon`)} /> },
                    {
                        title: 'MOVES',
                        onClick: () =>
                            dispatch.modal.openModal({
                                type: ModalConstants.MOVES,
                                data: pokemon[pokemonName]?.pokemon,
                                closable: true,
                            }),
                    },
                ]}
            />
        </>
    );
};

export default PokemonInformation;
