import request, { RequestResponse } from 'umi-request';
import { UrlPaginationType, NamedAPIResourceListType } from './server-utility-type';
import { CharacteristicType, Pokemon, PokemonSpeciesType } from './server-main-type';

export const getPokemonList = async (pagination?: UrlPaginationType): Promise<RequestResponse<NamedAPIResourceListType>> => {
    return request.get('/pokemon', {
        params: pagination,
        prefix: 'https://pokeapi.co/api/v2',
        getResponse: true,
    });
};

export const getPokemon = async (pokemonName: string): Promise<RequestResponse<Pokemon>> => {
    return request.get(`/pokemon/${pokemonName}`, {
        prefix: 'https://pokeapi.co/api/v2',
        getResponse: true,
    });
};

export const getPokemonSpecies = async (pokemonName: string): Promise<RequestResponse<PokemonSpeciesType>> => {
    return request.get(`/pokemon-species/${pokemonName}`, {
        prefix: 'https://pokeapi.co/api/v2',
        getResponse: true,
    });
};

export const getPokemonCharacteristic = async (highestStat: number): Promise<RequestResponse<CharacteristicType>> => {
    return request.get(`/characteristic/${highestStat / 5}`, {
        prefix: 'https://pokeapi.co/api/v2',
        getResponse: true,
    });
};
