import { DescriptionType, FlavorTextType, PokemonMoveType, PokemonStat, PokemonType } from './server-utility-type';

export interface Pokemon {
    id: number;
    name: string;
    base_experience: number;
    height: number;
    weight: number;
    moves: PokemonMoveType[];
    types: PokemonType[];
    stats: PokemonStat[];
    characteristics?: CharacteristicType;
}

export interface CharacteristicType {
    id: number;
    game_module: number;
    possible_values: number[];
    descriptions: DescriptionType[];
}

export interface PokemonSpeciesType {
    id: number;
    flavor_text_entries: FlavorTextType[];
}
