type PokemonMoveVersion = {
    level_learned_at: number;
    move_learned_at: NamedAPIResource;
    version_group: NamedAPIResource;
};

/*
    utility consumer
*/

export interface UrlPaginationType {
    limit: number;
    offset: number;
}

export interface NamedAPIResource {
    name: string;
    url: string;
}

export interface NamedAPIResourceListType {
    count: number | null;
    next: string | null;
    previous: boolean | null;
    results: NamedAPIResource[] | null;
}

export interface PokemonMoveType {
    move: NamedAPIResource;
    version_group_details: PokemonMoveVersion[];
}

export interface PokemonType {
    slot: number;
    type: NamedAPIResource;
}

export interface PokemonStat {
    stat: NamedAPIResource;
    effort: number;
    base_stat: number;
}

export interface DescriptionType {
    description: string;
    language: NamedAPIResource;
}

export interface FlavorTextType {
    flavor_text: string;
    language: NamedAPIResource;
    version: NamedAPIResource;
}
