import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Routes as BrowserRoutes, Route } from 'react-router-dom';

import { WebFontLoader } from './containers/WebFontLoader/WebFontLoader';
import { HomeLayout } from './layouts/HomeLayout/HomeLayout';
import { Home } from './pages/Home/Home';
import { PokemonInformation } from './pages/PokemonInformation/PokemonInformation';

import { store } from './store/store';

export const Routes = () => {
    return (
        <Provider store={store}>
            <WebFontLoader>
                <BrowserRouter>
                    <HomeLayout>
                        <BrowserRoutes>
                            <Route path={`/`} element={<Home />} />
                            <Route path={`/pokedex/:pokemonName/`} element={<PokemonInformation />} />
                            <Route path={`/teams/:pokemonName/`} element={<PokemonInformation />} />
                        </BrowserRoutes>
                    </HomeLayout>
                </BrowserRouter>
            </WebFontLoader>
        </Provider>
    );
};

export default Routes;
