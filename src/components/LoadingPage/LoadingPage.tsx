import React from 'react';
import Lottie from 'react-lottie';
import animationData from '../../assets/pikachu-loading.json';
import classNames from 'classnames';

import './LoadingPage.less';

const funcName = 'loading-page';

export const LoadingPage = () => {
    return (
        <div className={classNames(funcName)}>
            <Lottie
                options={{
                    loop: true,
                    autoplay: true,
                    animationData: animationData,
                    rendererSettings: { preserveAspectRatio: 'xMidYMid slice' },
                }}
                height={258}
                width={258}
            />
        </div>
    );
};

export default LoadingPage;
