import React from 'react';
import classNames from 'classnames';

import { Row, Col, Button } from 'antd';
import { ButtonProps } from 'antd/es/button';

import './SkewFooter.less';

const funcName = 'skew-footer';

interface SkewFooterActionType extends ButtonProps {
    title: string;
}

export interface SkewFooterType {
    dataSource: [SkewFooterActionType, SkewFooterActionType];
}

export const SkewFooter: React.FC<SkewFooterType> = ({ dataSource }) => {
    return (
        <div className={classNames(funcName)}>
            <div className={classNames(`${funcName}-background bShadow-1`)} />

            <Row className={classNames(`${funcName}-action-container`)}>
                <Col flex={1}>
                    <Button
                        className={classNames(`${funcName}-action-container-button`)}
                        style={{ color: '#000000' }}
                        type={'link'}
                        block={true}
                        {...dataSource[0]}
                    >
                        {dataSource[0].title}
                    </Button>
                </Col>
                <Col flex={1}>
                    <Button
                        className={classNames(`${funcName}-action-container-button`)}
                        type={'link'}
                        block={true}
                        {...dataSource[1]}
                    >
                        {dataSource[1].title}
                    </Button>
                </Col>
            </Row>
        </div>
    );
};

export default SkewFooter;
