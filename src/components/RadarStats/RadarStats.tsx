import React from 'react';
import DataSet from '@antv/data-set';
import { Chart, Point, Line, Area, Tooltip, Coordinate, useTheme } from 'bizcharts';

const data = [
    { item: 'Design', a: 70 },
    { item: 'Development', a: 60 },
    { item: 'Marketing', a: 50 },
    { item: 'Users', a: 40 },
    { item: 'Test', a: 60 },
    { item: 'Language', a: 70 },
];

function Demo() {
    const { DataView } = DataSet;
    const dv = new DataView().source(data);
    const [theme, setTheme] = useTheme('dark');
    dv.transform({
        type: 'fold',
        fields: ['a'],
        key: '',
        value: 'score',
    });

    return (
        <Chart
            height={400}
            data={dv.rows}
            autoFit
            scale={{
                score: {
                    min: 0,
                    max: 80,
                },
            }}
            theme={'light'}
            interactions={['legend-highlight']}
        >
            <Coordinate type="polar" radius={0.6} />
            <Tooltip shared />
            <Point position="item*score" color="#4D7BF3" shape="circle" />
            <Line position="item*score" color="#4D7BF3" size="3" />
            <Area position="item*score" color="#4D7BF3" />
        </Chart>
    );
}

export default Demo;
