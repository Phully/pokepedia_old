import React from 'react';
import classNames from 'classnames';

import { Card } from 'antd-mobile';
import { CardProps } from 'antd-mobile/es/card';
import { ReactComponent as CardBackground } from '../../assets/widget-card-background.svg';
import { ReactComponent as UltraBall } from '../../assets/ultraball.svg';

import './CatchCard.less';

const funcName = 'catch-card';

export interface CatchCardType extends CardProps {
    name: string;
    avatar: string;
    loading: boolean;
}

export const CatchCard: React.FC<CatchCardType> = ({ name, avatar, loading, onClick }) => {
    return (
        <Card id={funcName} className={classNames(funcName)} full={true} onClick={!loading ? onClick : () => null}>
            <CardBackground className={classNames(`${funcName}-catch-background`)} />
            {loading ? (
                <UltraBall className={classNames(`${funcName}-catch-skeleton`)} />
            ) : (
                <>
                    <img className={classNames(`${funcName}-catch-avatar`)} src={avatar} alt={'pokemon'} />
                    <span className={classNames(`${funcName}-catch-name`)}>{name}</span>
                </>
            )}
        </Card>
    );
};

export default CatchCard;
