import React from 'react';

import classNames from 'classnames';

import { ReactComponent as GonePikachu } from '../../assets/gone-pikachu.svg';

import { Result } from 'antd';

import './StatusPage.less';

const funcName = 'status-page';

export interface StatusPageType {
    type: 'no-data' | 'error' | 'success';
    title?: string;
    subTitle?: string;
}

export const StatusPage: React.FC<StatusPageType> = ({ type, title, subTitle }) => {
    return (
        <Result
            className={classNames(funcName)}
            icon={<GonePikachu height={150} width={150} style={{ marginLeft: 30 }} />}
            title={title}
            subTitle={subTitle}
        />
    );
};

export default StatusPage;
