import React from 'react';
import classNames from 'classnames';

import { ReactComponent as Banner } from '../../assets/banner.svg';

import './FavoriteWidget.less';

const funcName = 'favorite-widget';

export interface CatchCardType {
    title: string;
    subtitle: string;
    avatar: string;
}

export const FavoriteWidget: React.FC<CatchCardType> = ({ title, avatar, subtitle }) => {
    return (
        <div className={classNames(funcName)}>
            <Banner className={classNames(`${funcName}-banner`)} />
            <span className={classNames(`${funcName}-title`)}>{title}</span>
            <span className={classNames(`${funcName}-subtitle`)}>{subtitle}</span>
            <img alt={'pokemon'} className={classNames(`${funcName}-avatar`)} src={avatar} />
        </div>
    );
};

export default FavoriteWidget;
