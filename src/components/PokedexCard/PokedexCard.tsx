import React from 'react';
import classNames from 'classnames';

import { Card } from 'antd-mobile';
import { CardProps } from 'antd-mobile/es/card';
import { ReactComponent as PokedexCardBG } from '../../assets/pokedex-card-background.svg';

import './PokedexCard.less';

const funcName = 'pokedex-card';

export interface PokedexCardType extends CardProps {
    name: string;
    avatar: string;
}

export const PokedexCard: React.FC<PokedexCardType> = ({ name, avatar, onClick = () => null }) => {
    return (
        <Card id={funcName} className={classNames(funcName)} full={true} onClick={onClick}>
            <Card.Body>
                <div className={classNames(`${funcName}-body-container`)}>
                    <PokedexCardBG className={classNames(`${funcName}-background`)} />
                    <div className={classNames(`${funcName}-information`)}>
                        <img className={classNames(`${funcName}-avatar`)} alt={'pokemon'} src={avatar} />
                        <span className={classNames(`${funcName}-name`)}>{name}</span>
                    </div>
                </div>
            </Card.Body>
        </Card>
    );
};

export default PokedexCard;
