import React from 'react';
import classNames from 'classnames';
import { Button } from 'antd';

import { pokemonAvatar } from '../../utils/functions/pokemon-avatar';
import { Pokemon } from '../../services/server-main-type';

import './PokemonModal.less';

export interface PokemonModalType {
    dataSource: Pokemon;
    action?: {
        title: string;
        onClick: () => void;
    };
}

const funName = 'pokemon-modal';

export const PokemonModal: React.FC<PokemonModalType> = ({ dataSource, action }) => {
    return (
        <div className={classNames(funName)}>
            <img alt={'pokemon'} src={pokemonAvatar(dataSource.id.toString())} height={150} width={150} />
            <span className={classNames(`${funName}-name`)}>{dataSource.name}</span>
            {action && (
                <Button className={classNames(`${funName}-catch`)} type={'primary'} onClick={action?.onClick}>
                    {action?.title}
                </Button>
            )}
        </div>
    );
};

export default PokemonModal;
