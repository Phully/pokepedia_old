import React from 'react';
import classNames from 'classnames';
import { noTryAsync } from 'no-try';

import { Button, Form, Input } from 'antd';
import { ReactComponent as Gotcha } from '../../assets/gotcha.svg';

import './GotchaModal.less';

export interface GotchaModalType {
    serverRequest: any;
}

const funName = 'gotcha-modal';

export const GotchaModal: React.FC<GotchaModalType> = ({ serverRequest }) => {
    const [form] = Form.useForm();
    return (
        <div className={classNames(funName)}>
            <Form
                form={form}
                className={classNames(`${funName}-form-container`)}
                layout={'vertical'}
                initialValues={{ nickname: '' }}
                onFinish={async (values) => {
                    const [error] = await noTryAsync(() => serverRequest(values.nickname));
                    if (error) form.setFields([{ name: 'nickname', errors: [error.message] }]);
                }}
            >
                <Gotcha className={classNames(`${funName}-gotcha-icon`)} />
                <Form.Item
                    name={'nickname'}
                    rules={[
                        { required: true, message: 'Give me a name !!!' },
                        { min: 5, message: 'i dont like this name !!!' },
                    ]}
                >
                    <Input
                        className={classNames(`${funName}-input bShadow-4`)}
                        placeholder={'Give your pokemon a name'}
                        size={'large'}
                        bordered={false}
                        maxLength={20}
                    />
                </Form.Item>
                <Form.Item>
                    <Button className={classNames(`${funName}-button`)} type={'primary'} htmlType={'submit'}>
                        OK
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default GotchaModal;
