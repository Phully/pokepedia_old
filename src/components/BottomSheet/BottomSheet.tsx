import React, { useEffect, useRef } from 'react';
import { useDrag } from 'react-use-gesture';
import { useSpring, animated, config } from 'react-spring';
import { useSize } from '@umijs/hooks';
import classNames from 'classnames';

import { Tabs } from 'antd-mobile';

import './BottomSheet.less';

const funcName = 'bottom-sheet';

interface tabsDataSourceType {
    title: React.ReactNode;
    content: React.ReactNode;
}

export interface BottomSheetType {
    tabs: tabsDataSourceType[];
}

export const BottomSheet: React.FC<BottomSheetType> = ({ tabs }) => {
    const draggingRef = useRef(false);
    const [size] = useSize(() => document.querySelector('body'));
    const [{ y, scaleX }, set] = useSpring(() => ({ y: size.height - 45, scaleX: 0.88 }));

    const bind = useDrag(
        ({ first, last, vxvy: [, vy], movement: [, my], cancel }) => {
            if (first) draggingRef.current = true;
            else if (last) setTimeout(() => (draggingRef.current = false), 0);
            if (my < -70) cancel && cancel();
            if (last) my > size.height * 0.75 || vy > 0.5 ? open() : close();
            else set({ y: my, immediate: false, config: config.stiff });
        },
        { initial: () => [0, y.get()], filterTaps: true, bounds: { top: 0 }, rubberband: true }
    );

    const open = () => set({ y: 52, scaleX: 1 });

    const close = () => set({ y: size.height - 45, scaleX: 0.88 });

    useEffect(() => {
        set({ y: size.height - 45 });
    }, [size]);

    return (
        <animated.div className={classNames(funcName, 'bShadow-1')} style={{ bottom: -100, y, scaleX }}>
            <div className={classNames(`${funcName}-bangs-container`)} {...bind()}>
                <div className={classNames('lines')}>{null}</div>
            </div>
            <Tabs
                tabs={tabs.map(({ title }) => ({
                    title: (
                        <div className={classNames(`${funcName}-title-container`)} {...bind()}>
                            {title}
                        </div>
                    ),
                }))}
                initialPage={0}
                tabBarUnderlineStyle={{
                    color: '#fff',
                    backgroundColor: '#fff',
                    border: '0 solid #fff',
                }}
            >
                {React.Children.toArray(
                    tabs.map(({ content }) => <div className={classNames(`${funcName}-content-container`)}>{content}</div>)
                )}
            </Tabs>
        </animated.div>
    );
};

export default BottomSheet;
