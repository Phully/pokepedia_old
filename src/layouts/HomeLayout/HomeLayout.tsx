import React from 'react';
import classNames from 'classnames';

import { Layout } from 'antd';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

import { NavBar } from 'antd-mobile';
import { HomeOutlined } from '@ant-design/icons';
import { ModalWrapper } from '../../containers/ModalWrapper/ModalWrapper';
import { LoadingPage } from '../../components/LoadingPage/LoadingPage';
import { ReactComponent as Pokeball } from '../../assets/pokeball.svg';

import { RootState } from '../../store/store';

import './HomeLayout.less';

const funcName = 'home-layout';

export interface HomeLayout {
    children: React.ReactNode;
}

export const HomeLayout: React.FC<HomeLayout> = ({ children }) => {
    const navigate = useNavigate();
    const location = useLocation();
    const app = useSelector((state: RootState) => state.app);

    return (
        <Layout className={classNames(funcName)}>
            {location.pathname !== '/' && (
                <NavBar
                    className={classNames(`${funcName}-nav-bar`)}
                    icon={<HomeOutlined className={classNames(`${funcName}-nav-bar-home`)} />}
                    rightContent={[<Pokeball key={'pokeball'} className={classNames(`${funcName}-nav-bar-teams`)} />]}
                    onLeftClick={() => navigate('/')}
                />
            )}
            <div className={classNames(`${funcName}-content`)}>
                {app.page === 'loading' || app.font === 'loading' ? <LoadingPage /> : null}
                <div
                    className={classNames({
                        none: app.page === 'loading' || app.font === 'loading',
                    })}
                >
                    {children}
                </div>
            </div>
            <ModalWrapper />
        </Layout>
    );
};

export default HomeLayout;
