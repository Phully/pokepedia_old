import { createModel } from '@rematch/core';

import { AppConstant } from '../../utils/contansts/modal-constant';
import { getPokemon, getPokemonSpecies } from '../../services/pokemon';
import { randomPokemonId } from '../../utils/functions/random-pokemon-id';
import { randomPokemonIdRange } from '../../utils/functions/random-pokemon-id-range';
import { Pokemon, PokemonSpeciesType } from '../../services/server-main-type';
import { RootModel } from './root-model';

export interface WidgetCatchModelType {
    [key: string]: Pokemon | null;
}

export interface WidgetModelType {
    favorite: { pokemon: Pokemon; pokemonSpecies: PokemonSpeciesType };
    catch: WidgetCatchModelType;
}

export const widget = createModel<RootModel>()({
    state: { catch: {} } as WidgetModelType,
    reducers: {
        updateFavoriteWidget: (state, payload: { pokemon: Pokemon; pokemonSpecies: PokemonSpeciesType }) => ({
            ...state,
            favorite: payload,
        }),
        initialCatchWidget: (state, payload: WidgetCatchModelType) => ({
            ...state,
            catch: payload,
        }),
        updateCatchWidget: (state, payload: WidgetCatchModelType) => ({
            ...state,
            catch: { ...state.catch, ...payload },
        }),
    },
    effects: (dispatch) => ({
        async initiateFavoriteWidget() {
            const randomPokemon = randomPokemonIdRange(300, AppConstant.POKE_API_MAXIMUM_POKEMON);

            const pokemonResponse = await getPokemon(randomPokemon.toString());
            const pokemonSpeciesResponse = await getPokemonSpecies(pokemonResponse.data.name);

            dispatch.widget.updateFavoriteWidget({
                pokemon: pokemonResponse.data,
                pokemonSpecies: pokemonSpeciesResponse.data,
            });

            dispatch.pokemonInformation.addPokemonInformation({
                [pokemonResponse.data.name]: {
                    pokemon: pokemonResponse.data,
                    pokemonSpecies: pokemonSpeciesResponse.data,
                },
            });
        },
        async initiateCatchWidget() {
            const randomPokemon = randomPokemonId(5);

            dispatch.widget.initialCatchWidget(randomPokemon);

            Object.keys(randomPokemon).forEach((pokemonId) => {
                getPokemon(pokemonId).then((pokemonResponse) => {
                    getPokemonSpecies(pokemonResponse.data.name).then((pokemonSpeciesResponse) => {
                        dispatch.widget.updateCatchWidget({
                            [pokemonId]: pokemonResponse.data,
                        });
                        dispatch.pokemonInformation.addPokemonInformation({
                            [pokemonResponse.data.name]: {
                                pokemon: pokemonResponse.data,
                                pokemonSpecies: pokemonSpeciesResponse.data,
                            },
                        });
                    });
                });
            });
        },
    }),
});

export default widget;
