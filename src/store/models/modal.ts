import { createModel } from '@rematch/core';

import { RootModel } from './root-model';
import { ModalConstants } from '../../utils/contansts/modal-constant';

export interface ModalModelType {
    type: string;
    visibility?: boolean;
    closable?: boolean;
    data: any;
}

export const modal = createModel<RootModel>()({
    state: {
        type: ModalConstants.NO_MODAL,
        visibility: false,
        closable: false,
        data: {},
    } as ModalModelType,
    reducers: {
        openModal: (state, payload: ModalModelType) => ({
            ...state,
            ...payload,
            visibility: true,
        }),
        closeModal: (state) => ({
            ...state,
            visibility: false,
            closable: false,
        }),
    },
});

export default modal;
