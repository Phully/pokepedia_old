import { createModel } from '@rematch/core';
import _ from 'lodash';

import { getPokemonList } from '../../services/pokemon';
import { AppConstant } from '../../utils/contansts/modal-constant';

import { RootModel } from './root-model';
import { NamedAPIResource, UrlPaginationType } from '../../services/server-utility-type';

export interface PokemonModelType {
    pagination: UrlPaginationType;
    pokemonList: {
        [key: string]: NamedAPIResource;
    };
}

export const pokemon = createModel<RootModel>()({
    state: {
        pagination: {
            offset: 0,
            limit: AppConstant.POKE_API_OFFSET,
        },
        pokemonList: {},
    } as PokemonModelType,
    reducers: {
        updatePagination: (state, payload: UrlPaginationType) => ({
            ...state,
            pagination: payload,
        }),
        addPokemonList: (
            state,
            payload: {
                [key: string]: NamedAPIResource;
            }
        ) => ({
            ...state,
            pokemonList: {
                ...state.pokemonList,
                ...payload,
            },
        }),
    },
    effects: (dispatch) => ({
        async fetchPokemonList(params = null, state) {
            const pagination = state.pokemon.pagination;
            const maxOffset = AppConstant.POKE_API_MAXIMUM_POKEMON;
            const nextOffset = pagination.offset + pagination.limit;
            if (pagination.offset === maxOffset) {
                return;
            }
            const pokemonResponse = await getPokemonList(pagination);
            dispatch.pokemon.addPokemonList(_.keyBy(pokemonResponse.data.results, 'name'));
            dispatch.pokemon.updatePagination({
                offset: nextOffset,
                limit: nextOffset + pagination.limit >= maxOffset ? maxOffset - nextOffset : pagination.limit,
            });
        },
    }),
});

export default pokemon;
