import { createModel } from '@rematch/core';
import { RootModel } from './root-model';
import { Pokemon } from '../../services/server-main-type';

export interface MemberType {
    id: number;
    name: string;
    nickname: string;
}

export interface TeamsModelType {
    [key: string]: MemberType;
}

export const teams = createModel<RootModel>()({
    state: {} as TeamsModelType,
    reducers: {
        addMember: (state, payload: TeamsModelType) => ({ ...state, ...payload }),
        removeMember: (state, payload: TeamsModelType) => ({ ...payload }),
    },
    effects: (dispatch) => ({
        async catchMember(
            { pokemonNickname, pokemonInformation }: { pokemonNickname: string; pokemonInformation: Pokemon },
            state
        ) {
            if (state.teams[pokemonNickname]) {
                return Promise.reject(new Error('Hmm, i need another name :('));
            } else {
                dispatch.teams.addMember({
                    [pokemonNickname]: {
                        id: pokemonInformation.id,
                        name: pokemonInformation.name,
                        nickname: pokemonNickname,
                    },
                });
                dispatch.modal.closeModal();
            }
        },
        async releaseMember(pokemonNickname: string, state) {
            if (state.teams[pokemonNickname]) {
                delete state.teams[pokemonNickname];
                dispatch.teams.removeMember(state.teams);
            }
        },
    }),
});

export default teams;
