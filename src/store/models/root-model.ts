// @ts-nocheck
import { Models } from '@rematch/core';

import app from './app';
import teams from './teams';
import modal from './modal';
import widget from './widget';
import pokemon from './pokemon';
import pokemonInformation from './pokemon-information';

export interface RootModel extends Models<RootModel> {
    app: typeof app;
    teams: typeof teams;
    modal: typeof modal;
    widget: typeof widget;
    pokemon: typeof pokemon;
    pokemonInformation: typeof pokemonInformation;
}

export const rootModel: RootModel = { teams, pokemon, pokemonInformation, widget, app, modal };
