import { createModel } from '@rematch/core';

import { RootModel } from './root-model';

export interface AppModelType {
    page: 'loading' | 'loaded' | 'error';
    font: 'loading' | 'active' | 'fontinactive';
}

export const app = createModel<RootModel>()({
    state: { page: 'loading', font: 'loading' } as AppModelType,
    reducers: {
        pageStatus: (state, payload: 'loading' | 'loaded' | 'error') => ({
            ...state,
            page: payload,
        }),
        fontStatus: (state, payload: 'loading' | 'active' | 'fontinactive') => ({
            ...state,
            font: payload,
        }),
    },
});

export default app;
