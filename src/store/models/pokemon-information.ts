import { createModel } from '@rematch/core';

import { getPokemon, getPokemonSpecies } from '../../services/pokemon';
import { RootModel } from './root-model';
import { Pokemon, PokemonSpeciesType } from '../../services/server-main-type';

export interface informationSpeciesType {
    pokemon: Pokemon;
    pokemonSpecies: PokemonSpeciesType;
}

export interface PokemonInformationModelType {
    [key: string]: informationSpeciesType;
}

export const pokemonInformation = createModel<RootModel>()({
    state: {} as PokemonInformationModelType,
    reducers: {
        addPokemonInformation: (state, payload: PokemonInformationModelType) => ({
            ...state,
            ...payload,
        }),
    },
    effects: (dispatch) => ({
        async fetchPokemonInformation(pokemonName: string) {
            const pokemonResponse = await getPokemon(pokemonName);
            const pokemonSpeciesResponse = await getPokemonSpecies(pokemonName);
            dispatch.pokemonInformation.addPokemonInformation({
                [pokemonResponse.data.name]: { pokemon: pokemonResponse.data, pokemonSpecies: pokemonSpeciesResponse.data },
            });
        },
    }),
});

export default pokemonInformation;
