import { init, RematchDispatch, RematchRootState } from '@rematch/core';

import { rootModel, RootModel } from './models/root-model';

export const store = init({
    models: rootModel,
    redux: {
        devtoolOptions: { disabled: false },
    },
});

export type Dispatch = RematchDispatch<RootModel>;
export type RootState = RematchRootState<RootModel>;
