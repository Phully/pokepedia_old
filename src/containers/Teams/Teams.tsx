import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { ListView } from 'antd-mobile';
import { pokemonAvatar } from '../../utils/functions/pokemon-avatar';
import { PokedexCard } from '../../components/PokedexCard/PokedexCard';
import { StatusPage } from '../../components/StatusPage/StatusPage';

import { RootState } from '../../store/store';
import { MemberType } from '../../store/models/teams';

import './Teams.less';

const funcName = 'teams';

export const Teams: React.FC = () => {
    const navigate = useNavigate();
    const teamsData = useSelector((state: RootState) => state.teams);
    const [dataSource, setDataSource] = useState(new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 }));

    useEffect(() => {
        setDataSource(dataSource.cloneWithRows(teamsData));
    }, [teamsData]);

    return Object.keys(teamsData).length !== 0 ? (
        <ListView
            className={classNames(funcName)}
            dataSource={dataSource}
            useBodyScroll={false}
            pageSize={4}
            scrollRenderAheadDistance={500}
            renderRow={(rowData: MemberType) => (
                <PokedexCard
                    name={rowData.name}
                    avatar={pokemonAvatar(rowData.id.toString())}
                    onClick={() =>
                        navigate(`/teams/${rowData.name}`, {
                            state: {
                                member: rowData,
                            },
                        })
                    }
                />
            )}
            onEndReachedThreshold={10}
        />
    ) : (
        <StatusPage type={'no-data'} title={'gotta catch `em all!'} />
    );
};

export default Teams;
