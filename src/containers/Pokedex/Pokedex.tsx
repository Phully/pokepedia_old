import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRequest } from '@umijs/hooks';
import { useNavigate } from 'react-router-dom';
import _ from 'lodash';

import { ListView } from 'antd-mobile';
import { PokedexCard } from '../../components/PokedexCard/PokedexCard';

import { parsePokemonId } from '../../utils/functions/parse-pokemon-id';
import { pokemonAvatar } from '../../utils/functions/pokemon-avatar';
import { Dispatch, RootState } from '../../store/store';

import './Pokedex.less';

export const Pokedex: React.FC = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch<Dispatch>();
    const { pokemonList: pokedexData } = useSelector((state: RootState) => state.pokemon);
    const { run: fetchPokemon, loading: pokedexLoading } = useRequest(dispatch.pokemon.fetchPokemonList, { manual: true });
    const [dataSource, setDataSource] = useState(new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 }));

    useEffect(() => {
        if (!pokedexLoading) _.attempt(fetchPokemon);
    }, []);

    useEffect(() => {
        setDataSource(dataSource.cloneWithRows(pokedexData));
    }, [pokedexData]);

    return (
        <ListView
            className={'list-view'}
            dataSource={dataSource}
            useBodyScroll={false}
            pageSize={4}
            scrollRenderAheadDistance={500}
            renderRow={(rowData) => (
                <PokedexCard
                    name={rowData.name}
                    avatar={pokemonAvatar(parsePokemonId(rowData.url))}
                    onClick={() => navigate(`/pokedex/${rowData.name}`)}
                />
            )}
            onEndReachedThreshold={10}
            onEndReached={() => {
                if (!pokedexLoading) _.attempt(fetchPokemon);
            }}
        />
    );
};

export default Pokedex;
