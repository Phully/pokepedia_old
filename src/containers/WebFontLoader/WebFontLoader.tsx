import React from 'react';
import { useDispatch } from 'react-redux';

import WebfontLoader from '@dr-kobros/react-webfont-loader';

import { Dispatch } from '../../store/store';

export interface WebFontLoaderType {
    children: React.ReactNode;
}

export const WebFontLoader: React.FC<WebFontLoaderType> = ({ children }) => {
    const dispatch = useDispatch<Dispatch>();
    return (
        <WebfontLoader
            config={{
                google: {
                    families: ['Luckiest Guy', 'Concert One', 'cursive'],
                },
            }}
            onStatus={(status) => {
                if (status === 'loading' || status === 'active' || status === 'fontinactive') {
                    dispatch.app.fontStatus(status);
                }
            }}
        >
            {children}
        </WebfontLoader>
    );
};

export default WebFontLoader;
