import React from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';

import { Modal, Button } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import { ModalCollection } from '../../utils/component-mapper/modal-colection';

import { Dispatch, RootState } from '../../store/store';
import { ModalProps } from 'antd/es/modal';

import './ModalWrapper.less';

const funcName = 'modal-wrapper';

export interface ModalWrapperType extends ModalProps {}

export const ModalWrapper: React.FC<ModalWrapperType> = () => {
    const dispatch = useDispatch<Dispatch>();
    const modal = useSelector((state: RootState) => state.modal);

    const children = ModalCollection[modal.type](modal.data);

    return (
        <Modal
            className={classNames(funcName)}
            visible={modal.visibility}
            centered={true}
            title={null}
            footer={null}
            closable={false}
            maskClosable={false}
        >
            {modal.closable && (
                <Button
                    id={`${funcName}-close`}
                    className={classNames(`${funcName}-close`)}
                    type={'link'}
                    icon={
                        <CloseOutlined
                            id={`${funcName}-close-icon`}
                            className={classNames(`${funcName}-close-icon`)}
                            onClick={() => dispatch.modal.closeModal()}
                        />
                    }
                />
            )}
            <div className={classNames(`${funcName}-content-container`)}>{children}</div>
        </Modal>
    );
};

export default ModalWrapper;
