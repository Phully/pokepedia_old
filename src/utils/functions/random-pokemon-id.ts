export interface RandomPokemonId {
    [key: string]: null;
}

export const randomPokemonId = (lengthPokemon: number): RandomPokemonId => {
    let result = {};
    while (Object.keys(result).length < lengthPokemon) {
        const random = Math.floor(Math.random() * 649) + 1;
        result = {
            ...result,
            [random]: null,
        };
    }
    return result;
};
