/*
    this method for parse url pokeApi to get id of pokemon,
    because NamedAPIResource only return name and url,
    pokemonUrl : https://pokeapi.co/api/v2/pokemon/16/
    return pokemonId (Success)
    return null (failed)
*/
export const parsePokemonId = (pokemonUrl: string): string => {
    try {
        const { pathname } = new URL(pokemonUrl);
        const pokemonId = pathname.split('/')[4];
        if (Number.isNaN(Number(pokemonId))) {
            return '';
        }
        return pokemonId;
    } catch (e) {
        return '';
    }
};
