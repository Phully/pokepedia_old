export const probabilityCatchPokemon = (): boolean => {
    return Math.random() >= 0.5;
};
