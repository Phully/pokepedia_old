export abstract class AppConstant {
    static readonly POKE_API_MAXIMUM_POKEMON: number = 649;
    static readonly POKE_API_OFFSET: number = 50;
}

export abstract class ModalConstants {
    static readonly NO_MODAL: string = 'NO_MODAL';
    static readonly LOADING: string = 'LOADING';
    static readonly CATCH: string = 'CATCH';
    static readonly GOTCHA: string = 'GOTCHA';
    static readonly RELEASE: string = 'RELEASE';
    static readonly STATUS: string = 'STATUS';
    static readonly MOVES: string = 'MOVES';
}
