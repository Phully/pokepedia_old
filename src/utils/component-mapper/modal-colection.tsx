import React from 'react';

import { List } from 'antd';
import { LoadingPage } from '../../components/LoadingPage/LoadingPage';
import { PokemonModal } from '../../components/PokemonModal/PokemonModal';
import { GotchaModal } from '../../components/GotchModal/GotchaModal';
import { StatusPage } from '../../components/StatusPage/StatusPage';

import { ModalConstants } from '../contansts/modal-constant';
import { delay } from '../functions/delay';

import { store } from '../../store/store';
import { Pokemon } from '../../services/server-main-type';
import { PokemonMoveType } from '../../services/server-utility-type';
import { probabilityCatchPokemon } from '../functions/probability-catch-pokemon';

export const ModalCollection = {};

ModalCollection[ModalConstants.NO_MODAL] = (_: any): React.ReactNode => null;

ModalCollection[ModalConstants.LOADING] = (_: any): React.ReactNode => <LoadingPage />;

ModalCollection[ModalConstants.CATCH] = (pokemon: Pokemon): React.ReactNode => (
    <PokemonModal
        dataSource={pokemon}
        action={{
            title: ' Gotta Catch `Em all!',
            onClick: async () => {
                store.dispatch.modal.openModal({ type: ModalConstants.LOADING, data: pokemon, closable: false });
                await delay(1000);
                if (probabilityCatchPokemon()) {
                    store.dispatch.modal.openModal({ type: ModalConstants.GOTCHA, data: pokemon, closable: false });
                } else {
                    store.dispatch.modal.openModal({ type: ModalConstants.STATUS, data: 'error', closable: true });
                }
            },
        }}
    />
);

ModalCollection[ModalConstants.GOTCHA] = (pokemon: Pokemon): React.ReactNode => (
    <GotchaModal
        serverRequest={(nickname: string) => {
            return store.dispatch.teams
                .catchMember({
                    pokemonNickname: nickname,
                    pokemonInformation: pokemon,
                })
                .then(() => {
                    store.dispatch.modal.closeModal();
                });
        }}
    />
);

ModalCollection[ModalConstants.RELEASE] = ({ nickname, pokemon }: { nickname: string; pokemon: Pokemon }): React.ReactNode => (
    <PokemonModal
        dataSource={pokemon}
        action={{
            title: `Release ${nickname} ???`,
            onClick: async () => {
                store.dispatch.modal.openModal({ type: ModalConstants.LOADING, data: pokemon, closable: false });
                await delay(1000);
                store.dispatch.teams.releaseMember(nickname);
                await delay(450);
                store.dispatch.modal.closeModal();
            },
        }}
    />
);

ModalCollection[ModalConstants.STATUS] = (status: 'error' | 'success'): React.ReactNode => (
    <StatusPage type={status} title={status === 'error' ? 'One more time ~_~' : 'Yeeeahhh !!!!'} />
);

ModalCollection[ModalConstants.MOVES] = (pokemon: Pokemon): React.ReactNode => (
    <List
        header={<div className={'primary-color top-10'}>{pokemon.name} Moves : </div>}
        footer={null}
        bordered={false}
        dataSource={pokemon.moves}
        renderItem={({ move }: PokemonMoveType) => (
            <List.Item>
                <span>{move.name}</span>
            </List.Item>
        )}
    />
);

export default ModalCollection;
