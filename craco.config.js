const CracoLessPlugin = require('craco-less');

module.exports = {
    babel: {
        plugins: [
            [
                'import',
                {
                    libraryName: 'antd',
                    libraryDirectory: 'es',
                    style: true,
                },
                'antd',
            ],
            [
                'import',
                {
                    libraryName: 'antd-mobile',
                    libraryDirectory: 'es',
                    style: true,
                },
                'antd-mobile',
            ],
        ],
    },
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: {
                            '@font-family': `'Luckiest Guy', cursive, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'`,
                            '@primary-color': '#FF2400',
                            '@link-color': '#FF2400',
                            '@layout-body-background': '#ffffff',
                        },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
};
