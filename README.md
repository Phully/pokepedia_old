# Pokepedia

**Link ke project  (tidak responsive)**

[https://pokepedia.vercel.app/](https://pokepedia.vercel.app/)

Untuk catch dan relase datanya tersimpan pada redux, ketika halaman d refresh maka akan menghilangkan daftar teams, 
dan jika user sebelumnya masuk ke halaman teams information dengan cara redirect dari list teams dan user menakan tombol
refresh maka user akan d redirect kembali ke halaman home karena data teams sudah hilang. informasi teams dapat dilihat pada 
redux state (teams). saat ini saya belum menambahkan redux persist untuk menyimpan data catch

**redux devtools masih bisa dilihat di [https://pokepedia.vercel.app/](https://pokepedia.vercel.app/)**


[Gambar Redux State](https://ibb.co/DDPrB9k)

Note :
**16/09/2020** : terjadi beberapa perubahan pada style berdasarkan feedback test yang saya lakukan d real device android



Beberapa library yang digunakan : 

 1. Create react app
 2. Ant Design Mobile
 3. Umi Request
 4. Umi Hooks
 5. Rematch (Redux Framework)
 6. React Router Dom 
 
Beberapa tampilan project ketika d ambil menggunakan browser chrome dengan mode Device : 

[screen 1](https://imgur.com/RnIhFfS)

[screen 2](https://ibb.co/hggcYmh)

[screen 3](https://ibb.co/2yDt3Mz)

[screen 4](https://ibb.co/9Z4Pf2r)

[screen 5](https://ibb.co/L1ZLNH4)

**Note : tampilan untuk web tidak banyak di optimasi karena lebih berfokus kepada pembuatan component dan flow dari project ini.
       gambar diambil menggunakan browser chrome devtools tampilan iphone x dan pixel 2**




# Home page

[home screen](https://ibb.co/CmsNtSL)


 - [ ] terdapat banner informasi 1 pokemon dari poke api yang saya random 
 - [ ] terdapat informasi jumlah pokedex yang sudah di load dari pokeapi (50 of 649) terdapat pada bottom sheet tab 1 (pokedex) - mesti di drag
 - [ ] terdapat informasi jumlah pokemon yang sudah perna d tangkap list pokemon terdapat di bottom sheet tab kedua (teams) - mesti di drag
 - [ ] terdapat 5 pokemon yang d acak secara random dan tidak boleh ada duplikat di antara 5 pokemon carousel, (hanya 5 pokemon available to catch yang bisa d tangkap. saya berancana membuat 5 pokemon tersebut hanya bisa dapat d refresh setiap 10 menit sekali dan berencana menyimpan di redux persist tapi karena keterbatasan waktu akhirnya saya batalkan)
 - [ ] bottom sheet yang bisa d drag berisi list pokedex dan list teams

# Pokemon Information

[pokemon information](https://ibb.co/2yDt3Mz)

 - [ ] terdapat informasi seputar id, nama, ability, deskripsi dan juga move dari pokemon
 - [ ] terdapat button release yang hanya tampil jika user membuka halaman pokemon informasi dari list teams, jika di click user akan d arahkan ke halaman home


# Structure project

[structure project](https://ibb.co/rbVgGL1)


